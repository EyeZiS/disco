// ==UserScript==
// @name         WSP Transaction Log CSS Fixes
// @namespace    http://udf.github.io/
// @version      0.2
// @description  Some style fixes for the WSPTransactionLog page
// @author       Sameer Hoosen
// @match        http://dbdtfs01.resource.discoverybank.co.za/WSPTransactionLog/Home
// @grant        none
// @updateURL    https://bitbucket.org/EyeZiS/disco/raw/master/WSP_css_fixes.js
// ==/UserScript==

let MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

(function() {
    'use strict';

    // remove shadow thingy
    document.styleSheets[0].addRule('#box:before', 'display: none;');
    document.styleSheets[0].addRule('#box:after', 'display: none;');

    // make textareas monospace
    document.styleSheets[0].addRule('textarea', 'font-family: monospace !important;');

    // make new textareas in the search results read only
    // fix background color and cursor
    document.styleSheets[0].addRule('textarea', 'background-color: unset !important;');
    document.styleSheets[0].addRule('textarea', 'cursor: unset !important;');
    // set spellcheck to false on every new textarea
    let observer = new MutationObserver(function(mutations, observer) {
        let elements = mutations[0].target.getElementsByTagName('textarea');
        for (let element of elements) {
            element.setAttribute('spellcheck', 'false')
        }
    });
    observer.observe(
        document.getElementById('searchResults'),
        {
            childList: true
        }
    );
})();